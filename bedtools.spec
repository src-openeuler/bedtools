Name:		bedtools
Version:	2.31.1
Release:	1
Summary:	A flexible suite of utilities for comparing genomic features
License:	MIT
URL:		https://github.com/arq5x/bedtools2
Source0:	https://github.com/arq5x/bedtools2/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++ make zlib zlib-devel bzip2-devel xz-devel python3 python3-sphinx

%description
The BEDTools utilities allow one to address common genomics tasks such
as finding feature overlaps and computing coverage. The utilities are
largely based on four widely-used file formats: BED, GFF/GTF, VCF, and
SAM/BAM. Using BEDTools, one can develop sophisticated pipelines that
answer complicated research questions by "streaming" several BEDTools
together. 


%prep
%setup -q -n bedtools2
# remove bundled curl library
rm -rf src/utils/curl

%build
%make_build

%install
mkdir -p %{buildroot}%{_bindir}
install -m 0755 bin/* %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_datadir}/%{name}
cp -a genomes/ %{buildroot}%{_datadir}/%{name}
cp -a data/ %{buildroot}%{_datadir}/%{name}
find %{buildroot}%{_datadir}/%{name} -type f -exec chmod 0644 {} \;

%files
%defattr(-,root,root,-)
%doc README.md  LICENSE
%dir %{_datadir}/%{name}
%{_bindir}/*

%{_datadir}/%{name}/genomes
%{_datadir}/%{name}/data


%changelog
* Fri Dec 15 2023 Vicoloa <lvkun@uniontech.com> - 2.31.1-1
- update to 2.31.1

* Sat Jun 3 2023 guoyizhang <kuoi@bioarchlinux.org> 2.31.0-1
- update to 2.31.0

* Mon Apr 11 2022 herengui <herengui@uniontech.com> - 2.30.0-3
- add necessary buildrequires.

* Thu May 6 2021 duyiwei <duyiwei@kylinos.cn> - 2.30.0-2
- fix the problem that python2 and python3 cannot be distinguished

* Fri Apr 30 2021 duyiwei <duyiwei@kylinos.cn> - 2.30.0-1
- init package

